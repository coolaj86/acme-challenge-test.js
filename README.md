# [acme-challenge-test](https://git.rootprojects.org/root/acme-challenge-test.js.git) | a [Root](https://rootprojects.org) project

Test harness for ACME http-01 and dns-01 challenges for Let's Encrypt Free SSL integration.

This was specificially designed for [ACME.js](https://git.coolaj86.com/coolaj86/acme-v2.js) and [Greenlock.js](https://git.coolaj86.com/coolaj86/greenlock-express.js), but will be generically useful to any ACME module.

> If you are building a plugin, please let us know.
> We may like to co-author and help maintain and promote your module.

This package has been split in two for the purpose of keeping the documentation clear and concise.

## ACME http-01

Use this for quick-and-easy, average-joe kind of stuff.

- See <https://git.rootprojects.org/root/acme-http-01-test.js>

## ACME dns-01

Use this for wildcards, and private and local domains.

- See <https://git.rootprojects.org/root/acme-dns-01-test.js>

## Reference Implementations

These are plugins that use the v2.7+ (v3) API, and pass this test harness,
which you should use as a model for any plugins that you create.

- http-01
  - [`acme-http-01-cli`](https://git.rootprojects.org/root/acme-http-01-cli.js)
  - [`acme-http-01-fs`](https://git.rootprojects.org/root/acme-http-01-fs.js)
- dns-01
  - [`acme-dns-01-cli`](https://git.rootprojects.org/root/acme-dns-01-cli.js)
  - [`acme-dns-01-digitalocean`](https://git.rootprojects.org/root/acme-dns-01-digitalocean.js)

You can find other implementations by searching npm for [acme-http-01-](https://www.npmjs.com/search?q=acme-http-01-)
and [acme-dns-01-](https://www.npmjs.com/search?q=acme-dns-01-).

## Starter Template

Just so you have an idea, this is typically how you'd start passing the tests:

```js
var tester = require('acme-challenge-test');

// The dry-run tests can pass on, literally, 'example.com'
// but the integration tests require that you have control over the domain
var domain = 'example.com';

tester
  .testRecord('http-01', domain, {
    set: function(opts) {
      console.log('set opts:', opts);
      throw new Error('set not implemented');
    },

    remove: function(opts) {
      console.log('remove opts:', opts);
      throw new Error('remove not implemented');
    },

    get: function(opts) {
      console.log('get opts:', opts);
      throw new Error('get not implemented');
    }
  })
  .then(function() {
    console.info('PASS');
  });
```
